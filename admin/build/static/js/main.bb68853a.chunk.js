(this["webpackJsonpdiploma-work"] =
  this["webpackJsonpdiploma-work"] || []).push([
  [0],
  {
    12: function (e, a, t) {},
    16: function (e, a, t) {
      e.exports = t(27);
    },
    21: function (e, a, t) {},
    25: function (e, a, t) {
      e.exports = t.p + "static/media/giphy.88047f8d.gif";
    },
    27: function (e, a, t) {
      "use strict";
      t.r(a);
      var n = t(0),
        r = t.n(n),
        o = t(14),
        l = t.n(o),
        c = (t(21), t(12), t(5)),
        s = t(6),
        i = t(29),
        m = t(10),
        u = t(8);
      var d = function () {
        var e = Object(n.useState)(!0),
          a = Object(s.a)(e, 2),
          t = a[0],
          o = a[1],
          l = Object(n.useState)({ email: "", password: "" }),
          d = Object(s.a)(l, 2),
          p = d[0],
          E = d[1],
          w = Object(n.useState)({
            email: "",
            password: "",
            repeatPassword: "",
          }),
          g = Object(s.a)(w, 2),
          v = g[0],
          f = g[1],
          h = Object(u.validateEmail)(p.email),
          b =
            Object(u.validateEmail)(v.email) && v.password === v.repeatPassword;
        return r.a.createElement(
          "div",
          { className: "Form-container" },
          r.a.createElement("h3", null, t ? "Login" : "Create Account"),
          r.a.createElement(
            i.a,
            null,
            t
              ? r.a.createElement(
                  r.a.Fragment,
                  null,
                  r.a.createElement(
                    i.a.Group,
                    { controlId: "formBasicEmail" },
                    r.a.createElement(i.a.Label, null, "Email address"),
                    r.a.createElement(i.a.Control, {
                      type: "email",
                      placeholder: "Enter email",
                      value: p.email,
                      onChange: function (e) {
                        return E(Object(c.a)({}, p, { email: e.target.value }));
                      },
                    })
                  ),
                  r.a.createElement(
                    i.a.Group,
                    { controlId: "formBasicPassword" },
                    r.a.createElement(i.a.Label, null, "Password"),
                    r.a.createElement(i.a.Control, {
                      type: "password",
                      value: p.password,
                      placeholder: "Password",
                      onChange: function (e) {
                        return E(
                          Object(c.a)({}, p, { password: e.target.value })
                        );
                      },
                    })
                  )
                )
              : r.a.createElement(
                  r.a.Fragment,
                  null,
                  r.a.createElement(
                    i.a.Group,
                    { controlId: "formBasicEmail" },
                    r.a.createElement(i.a.Label, null, "Email address"),
                    r.a.createElement(i.a.Control, {
                      type: "email",
                      placeholder: "Enter email",
                      value: v.email,
                      onChange: function (e) {
                        return f(Object(c.a)({}, v, { email: e.target.value }));
                      },
                    }),
                    r.a.createElement(
                      i.a.Text,
                      { className: "text-muted" },
                      "We'll never share your email with anyone else."
                    )
                  ),
                  r.a.createElement(
                    i.a.Group,
                    { controlId: "formBasicPassword" },
                    r.a.createElement(i.a.Label, null, "Password"),
                    r.a.createElement(i.a.Control, {
                      type: "password",
                      value: v.password,
                      placeholder: "Password",
                      onChange: function (e) {
                        return f(
                          Object(c.a)({}, v, { password: e.target.value })
                        );
                      },
                    })
                  ),
                  r.a.createElement(
                    i.a.Group,
                    { controlId: "formBasicPassword" },
                    r.a.createElement(i.a.Label, null, "Repeat Password"),
                    r.a.createElement(i.a.Control, {
                      type: "password",
                      value: v.repeatPassword,
                      placeholder: "Password",
                      onChange: function (e) {
                        return f(
                          Object(c.a)({}, v, { repeatPassword: e.target.value })
                        );
                      },
                    })
                  )
                ),
            r.a.createElement(
              "div",
              { className: "Button-container" },
              r.a.createElement(
                m.a,
                {
                  variant: "primary",
                  onClick: function () {
                    o(!t);
                  },
                },
                t ? "Create Account" : "Sing In"
              ),
              r.a.createElement(
                m.a,
                {
                  disabled: t ? !h : !b,
                  variant: "primary",
                  type: "submit",
                  onClick: function () {
                    return t ? console.log(p) : console.log(v);
                  },
                },
                t ? "Login" : "Sing Up"
              )
            )
          )
        );
      };
      var p = function () {
        return r.a.createElement(
          "div",
          { className: "Main-container" },
          r.a.createElement(
            "div",
            { className: "Logo-container" },
            r.a.createElement("img", {
              className: "Main-logo",
              src: t(25),
              alt: "logo",
            })
          ),
          d()
        );
      };
      Boolean(
        "localhost" === window.location.hostname ||
          "[::1]" === window.location.hostname ||
          window.location.hostname.match(
            /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
          )
      );
      t(26);
      l.a.render(
        r.a.createElement(r.a.StrictMode, null, r.a.createElement(p, null)),
        document.getElementById("root")
      ),
        "serviceWorker" in navigator &&
          navigator.serviceWorker.ready
            .then(function (e) {
              e.unregister();
            })
            .catch(function (e) {
              console.error(e.message);
            });
    },
    8: function (e, a, t) {
      e.exports = t.p + "static/media/Utils.d51707e3.bin";
    },
  },
  [[16, 1, 2]],
]);
//# sourceMappingURL=main.bb68853a.chunk.js.map
