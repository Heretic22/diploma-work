import React, {useEffect, useState} from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import EventCard from "./EventCard";
import sessionController from '../services/sessionController'
import MainContainer from "./MainContainer";
import eventsRepository from "../repository/eventsRepository";


const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);



const useStyles = makeStyles({
    table: {
        minWidth: 700,
    },
});

export default function EventsList() {
    const classes = useStyles();
    const [events, setEvents] = useState([]);


    useEffect(()=>{
        fetch('http://localhost:8443/api/events/school-events',
            {method:'GET',headers: { "Content-Type": "application/json", 'authorization': sessionController.getSession()}})
            .then(async (result) => {
                console.log(result.status);
                setEvents(await result.json());
            });
        console.log(eventsRepository);
    },[]);

    return (
        <MainContainer>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell/>
                            <StyledTableCell align="right">Event Name</StyledTableCell>
                            <StyledTableCell align="right">Status</StyledTableCell>
                            <StyledTableCell align="right">Start Date</StyledTableCell>
                            <StyledTableCell align="right">Photo</StyledTableCell>
                            <StyledTableCell align="right"/>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {events.map((item) => (
                            <EventCard key={item.id} event={item}/>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </MainContainer>
    );
}