import React from "react";
import "../styles/Main.css";
import Avatar from "@material-ui/core/Avatar";
import { makeStyles } from '@material-ui/core/styles';
import withStyles from "@material-ui/core/styles/withStyles";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import SimpleMenu from "./Menu";
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import IconButton from "@material-ui/core/IconButton";
import Collapse from "@material-ui/core/Collapse";
import Box from "@material-ui/core/Box";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableHead from "@material-ui/core/TableHead";
import Typography from "@material-ui/core/Typography";
import {observer} from "mobx-react";
import eventsRepository from "../repository/eventsRepository";


const EventCard = observer(({event}) =>{

    const StyledTableCell = withStyles((theme) => ({
        head: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        body: {
            fontSize: 14,
        },
    }))(TableCell);
    const StyledTableRow = withStyles((theme) => ({
        root: {
            '&:nth-of-type(odd)': {
                backgroundColor: theme.palette.action.hover,
            },
        },
    }))(TableRow);


    const useStyles = makeStyles((theme) => ({
        large: {
            width: theme.spacing(10),
            height: theme.spacing(10),
        },
    }));

    const [open, setOpen] = React.useState(false);
    const classes = useStyles();

    return(
        <React.Fragment>
            <StyledTableRow>
                <TableCell component="th" scope="row">
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <StyledTableCell align="right">{event.name}</StyledTableCell>
                <StyledTableCell align="right">{event.status}</StyledTableCell>
                <StyledTableCell align="right">{event.start}</StyledTableCell>
                <StyledTableCell align="right">
                    <div className={'Event-card-img'}>
                        <Avatar alt="Remy Sharp" src={event.photoUrl} className={classes.large}/>
                    </div>
                </StyledTableCell>
                <TableCell align="right">
                    <SimpleMenu id={event.id}/>
                </TableCell>
            </StyledTableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={1}>
                            <Typography variant="h6" gutterBottom component="div">
                                Event Data
                            </Typography>
                            <Table size="small" aria-label="purchases">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>{eventsRepository.selectedEvent.name}</TableCell>
                                        <TableCell>Event Duration</TableCell>
                                        <TableCell>Address</TableCell>
                                        <TableCell align="right">Category</TableCell>
                                        <TableCell align="right">Description</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow >
                                        <TableCell component="th" scope="row">
                                            {'historyRow.date'}
                                        </TableCell>
                                        <TableCell>{'historyRow.customerId'}</TableCell>
                                        <TableCell>{'historyRow.amount'}</TableCell>
                                        <TableCell align="right">{'historyRow.amount'}</TableCell>
                                        <TableCell align="right">
                                            {"Math.round(historyRow.amount * row.price * 100) / 100Math.round(historyRow.amount * row.price * 100) / 100Math.round(historyRow.amount * row.price * 100) / 100Math.round(historyRow.amount * row.price * 100) / 100Math.round(historyRow.amount * row.price * 100) / 100"}
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    )
})

export default EventCard;