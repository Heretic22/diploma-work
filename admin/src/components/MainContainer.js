import React, {useState} from "react";
import Button from "react-bootstrap/Button";
import sessionController from "../services/sessionController";
import history from "../services/history";
import "../styles/Main.css";


function MainContainer(props) {

    const buttons = [{title:'Events', path:'/events'},{title:'Create Event', path:'/event_create'},{title:'Students', path:'/students'}, {title:'Analysis', path:'/analysis'}]

    return(
        <div className={"Main-container"}>
            <div className={'Menu-container'}>
                <div className="nav flex-column nav-pills Buttons-container" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    {buttons.map(item=> {
                        const className = "menuButton nav-item nav-link" + (history.location.pathname === item.path ? ' active' : '');
                        return (<button key={item.path} className={className}
                               onClick={()=>history.push(item.path)}>{item.title}</button>)
                        }
                    )}
                </div>
                <div className={'Logout-container'}>
                    <Button variant="danger" onClick={()=>{
                        sessionController.deleteSession();
                        history.push('/');
                    }}>
                        {'Logout'}
                    </Button>
                </div>
            </div>
            <div className={'Children-container'}>
                {props.children}
            </div>
        </div>
    )

}

export default MainContainer;
