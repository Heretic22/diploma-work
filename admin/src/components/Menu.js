import React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from '@material-ui/icons/MoreVert';
import sessionController from "../services/sessionController";
import {observer} from "mobx-react";
import eventsRepository from "../repository/eventsRepository";
import EventCard from "./EventCard";

const SimpleMenu = observer(({id})=> {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    const handleProfile = () => {
        setAnchorEl(null);
        fetch('http://localhost:8443/api/events/get-event-by-id/'+id,
            {method:'GET',headers: { "Content-Type": "application/json", 'authorization': sessionController.getSession()}})
            .then(res=>res.json())
            .then(async (result) => {
                console.log(result.status);
                eventsRepository.selectedEvent = result;
            })
    };
    const handleUpdate = () => {
        setAnchorEl(null);
    };
    const handleDelete = () => {
        setAnchorEl(null);
    };

    return (
        <div>
            <IconButton
                aria-label="more"
                aria-controls="long-menu"
                aria-haspopup="true"
                onClick={handleClick}
            >
                <MoreVertIcon />
            </IconButton>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={handleProfile}>Open</MenuItem>
                <MenuItem onClick={handleUpdate}>Update</MenuItem>
                <MenuItem onClick={handleDelete}>Delete</MenuItem>
            </Menu>
        </div>
    );
});

export default SimpleMenu;