import React, {useState} from "react";
import { Form } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import "../styles/App.css";
import history from "../services/history";
import sessionController from '../services/sessionController'

function SingInForm() {
  const [loginForm, setLoginForm] = useState(true);
  const [loginData, setLoginData] = useState({
    email: "",
    password: "",
  });
  const [createData, setCreateData] = useState({
    email: "",
    password: "",
    repeatPassword: "",
      fullName:'',
  });
  const validateEmail = (email) => {
    return (
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@([a-zA-Z0-9]+)\.([a-zA-Z0-9]+){2,}/.test(
        email
      ) && !/ /.test(email)
    );
  };



  const validateLogin = validateEmail(loginData.email) && loginData.password;
  const validateCreate =
    validateEmail(createData.email) &&
      createData.fullName &&
    createData.password.length &&
    createData.password === createData.repeatPassword;

  const change = () => {
    setLoginForm(!loginForm);
  };

  return (
    <div className={"Form-container"}>
      <h3>{loginForm ? "Login" : "Create Account"}</h3>
      <Form>
        {loginForm ? (
          <>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={loginData.email}
                onChange={(event) =>
                  setLoginData({ ...loginData, email: event.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                value={loginData.password}
                placeholder="Password"
                onChange={(event) =>
                  setLoginData({ ...loginData, password: event.target.value })
                }
              />
            </Form.Group>
          </>
        ) : (
          <>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={createData.email}
                onChange={(event) =>
                  setCreateData({ ...createData, email: event.target.value })
                }
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
                <Form.Label>Full name</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter your full name"
                    value={createData.fullName}
                    onChange={(event) =>
                        setCreateData({ ...createData, fullName: event.target.value })
                    }
                />
            </Form.Group>
            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                value={createData.password}
                placeholder="Password"
                onChange={(event) =>
                  setCreateData({ ...createData, password: event.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="formBasicPassword">
              <Form.Label>Repeat Password</Form.Label>
              <Form.Control
                type="password"
                value={createData.repeatPassword}
                placeholder="Password"
                onChange={(event) =>
                  setCreateData({
                    ...createData,
                    repeatPassword: event.target.value,
                  })
                }
              />
            </Form.Group>
          </>
        )}
        <div className={"Button-container"}>
          <Button variant="primary" onClick={change}>
            {loginForm ? "Create Account" : "Sing In"}
          </Button>
          <Button
            disabled={loginForm ? !validateLogin : !validateCreate}
            variant="primary"
            onClick={() => {
              const data = loginForm ? loginData : {email: createData.email, password:createData.password, fullName:createData.fullName};
              const endpoint = loginForm?'login':'register';
              fetch('http://localhost:8443/api/auth/'+endpoint,{method:'POST',headers: { "Content-Type": "application/json" }, body: JSON.stringify(data)})
                  .then(async (result) => {
                    console.log(result.status);
                    const data = await result.json();
                    console.log(data);
                      if(result.status === 200){
                          sessionController.createSession(data.success);
                          history.push(data.school?'/events':'/create_school')
                      }
                  })
            }}
          >
            {loginForm ? "Login" : "Sing Up"}
          </Button>
        </div>
      </Form>
    </div>
  );
}

export default SingInForm;
