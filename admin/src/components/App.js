import React from "react";
import "../styles/App.css";
import SingInForm from "./SignForm";
import { Router, Route, Redirect } from "react-router-dom";
import history from "../services/history.js";
import sessionController from '../services/sessionController'
import SchoolCreateForm from "./SchoolCreateForm";
import MainContainer from "./MainContainer";
import CreateEvent from "../components/CreateEvent";
import Events from "./EventsList";



const auth = ()=>{
    return (
        <div className={"Container"}>
            <div className={"Logo-container"}>
                <img className={"Main-logo"} src={require("../giphy.gif")} alt="logo" />
            </div>
            <SingInForm/>
        </div>
    )
};

const main = ()=>{
    return (<MainContainer>
        <button onClick={() => {
            sessionController.deleteSession();
            history.push('/');
        }}>Logout</button>
    </MainContainer>)
};

function App() {

    const handleSession = (component) =>{
        return sessionController.getSession()?
            component:
            <Redirect to="/auth"/>
    };

  return (
      <Router history={history}>
          <Route exact path="/" render={() =>
              sessionController.getSession()?
                  <Redirect to="/events"/>:
                  <Redirect to="/auth"/>
          }/>
          <Route name={'auth'} path={'/auth'} render={auth}/>
          <Route name={'events'} path={'/events'} render={() => handleSession(<Events/>)}/>
          <Route name={'create_school'} path={'/create_school'} render={() => handleSession(<SchoolCreateForm/>)}/>
          <Route name={'event_create'} path={'/event_create'} render={() => handleSession(<CreateEvent/>)}/>
          <Route name={'students'} path={'/students'} render={() => handleSession(main())}/>
          <Route name={'event_create'} path={'/analysis'} render={() => handleSession(main())}/>
      </Router>
  );
}

export default App;
