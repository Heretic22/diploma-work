import React, {useState} from "react";
import { Form } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import sessionController from "../services/sessionController";
import history from "../services/history";



function SchoolCreateForm() {
    const [createData, setCreateData] = useState({
        fullName: "",
        requiredHours:0
    });

    const validate = createData.fullName && createData.requiredHours;

    return(
        <div className={"Main-container"}>
            <div className={"Form-container"}>
                <h3>{"Create School"}</h3>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>School Full name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter your school full name"
                        value={createData.fullName}
                        onChange={(event) =>
                            setCreateData({ ...createData, fullName: event.target.value })
                        }
                    />
                    <Form.Label>Number of required hours</Form.Label>
                    <Form.Control
                        type="number"
                        pattern="[0-9]*"
                        placeholder="Enter number of required hours"
                        value={createData.requiredHours}
                        onChange={(event) =>
                            setCreateData({ ...createData, requiredHours: event.target.value*1 })
                        }
                    />
                </Form.Group>
                <Button disabled={!validate} variant="primary" onClick={()=>{
                    fetch('http://localhost:8443/api/schools/create',
                        {method:'POST',headers: { "Content-Type": "application/json", 'authorization': sessionController.getSession()}, body: JSON.stringify(createData)})
                        .then(async (result) => {
                            console.log(result.status);
                            const data = await result.json();
                            console.log(data);
                            if(result.status === 200){
                                history.push('/events');
                            }
                        })
                }}>
                    {"Create"}
                </Button>
            </div>
        </div>
    )
}

export default SchoolCreateForm;
