import React, {useCallback, useState} from "react";
import Button from "react-bootstrap/Button";
import "../styles/Main.css";
import MainContainer from "./MainContainer";
import {Form} from "react-bootstrap";
import DateTimePicker from 'react-datetime-picker';
import Gallery from "react-photo-gallery";
import SelectedImage from "./SelectedImage";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import sessionController from '../services/sessionController'
import history from "../services/history";





function CreateEvent() {
    const categories = [
        'Animals',
        'Human rights',
        "Children's",
        'Civil',
        'Health',
        'Veterans',
        'Specials needs'
    ];

    const [createData, setCreateData] = useState({
        name: "",
        address: "",
        startDate: new Date(),
        endDate: new Date(new Date().valueOf() + 3600000),
        photoUrl:'',
        category:categories[0],
        description:''
    });
    const [search, setSearch] = useState('');
    const [images, setImages] = useState([]);
    const startOnChange = (date) =>{
        setCreateData({...createData, startDate:date, endDate:createData.endDate.valueOf()<date.valueOf()?new Date(date.valueOf() + 3600000):createData.endDate});
    };
    const endOnChange = (date) =>{
        if(date.valueOf() < createData.startDate.valueOf()){
            date = new Date(createData.startDate.valueOf() + 3600000);
        }
        setCreateData({...createData, endDate:date});
    };


    const imageRenderer = useCallback(
        ({ index, left, top, key, photo }) => (
            <SelectedImage
                key={key}
                margin={"2px"}
                index={index}
                photo={photo}
                left={left}
                top={top}
                handleOnPress={data=>setCreateData({...createData, photoUrl:data})}
            />
        )
    );

    const validate = createData.name.length &&
        createData.address.length &&
        createData.startDate &&
        createData.endDate &&
        createData.category.length &&
        createData.description.length &&
        createData.photoUrl.length;

    return(
        <MainContainer>
            <div>
                <div className={'Title'}>
                    <h1>Create new EVENT</h1>
                    <Button disabled={!validate}  variant="primary" onClick={()=>{
                        console.log(createData);
                        fetch('http://localhost:8443/api/events/create',
                            {method:'POST',headers: { "Content-Type": "application/json", 'authorization': sessionController.getSession()}, body: JSON.stringify(createData)})
                            .then(async (result) => {
                                console.log(result.status);
                                const data = await result.json();
                                console.log(data);
                                if(result.status === 200){
                                    history.push('/events');
                                }
                            })
                    }}>
                        {"Create"}
                    </Button>
                </div>
                <div className={"Form-container create-form"}>
                    <h3>Please enter all fields</h3>
                    <Form.Group controlId="name">
                        <Form.Label>Event name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter Event name"
                            value={createData.name}
                            onChange={(event) =>
                                setCreateData({ ...createData, name: event.target.value })
                            }
                        />
                    </Form.Group>
                    <Form.Group controlId="address">
                        <Form.Label>Event address</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter event address"
                            value={createData.address}
                            onChange={(event) =>
                                setCreateData({ ...createData, address: event.target.value })
                            }
                        />
                    </Form.Group>
                    <Form.Label>Start date and time</Form.Label>
                    <DateTimePicker
                        minDate={new Date()}
                        onChange={(data)=>startOnChange(data)}
                        value={createData.startDate}
                    />
                    <Form.Label>End date and time</Form.Label>
                    <DateTimePicker
                        minDate={createData.startDate}
                        onChange={(data)=>endOnChange(data)}
                        value={createData.endDate}
                    />
                    <Form.Group controlId="category">
                        <Form.Label>Category</Form.Label>
                        <Form.Control as="select" onChange={(event)=>setCreateData({ ...createData, category: event.target.value })}>
                            {categories.map(item=><option key={item}>{item}</option>)}
                        </Form.Control>
                    </Form.Group>
                    <Form.Group controlId="description">
                        <Form.Label>Description</Form.Label>
                        <Form.Control as="textarea" rows="3" onChange={(event)=>setCreateData({ ...createData, description: event.target.value })} />
                    </Form.Group>
                </div>
                <Form.Label>Search and select photo</Form.Label>
                <InputGroup className="mb-3">
                    <FormControl
                        placeholder="Recipient's username"
                        aria-label="Recipient's username"
                        aria-describedby="basic-addon2"
                        onChange={(event)=>setSearch(event.target.value)}
                    />
                    <InputGroup.Append>
                        <Button variant="outline-secondary" onClick={()=>{
                            fetch(`https://api.unsplash.com/search/photos/?page=1&per_page=10&query=${search}&client_id=NpHsUdW9Ql3ZXdYb-lv6SAdbyHdDTFdO4gu03mkFh8I`)
                                .then(res => res.json())
                                .then(data => {
                                    setImages(data.results.map(item=>{
                                        return {
                                            src:item.urls.small,
                                            width: item.width*0.4,
                                            height: item.height*0.4
                                        }}));
                                })
                                .catch(err => {
                                    console.log('Error happened during fetching!', err)});
                        }}>Search</Button>
                    </InputGroup.Append>
                </InputGroup>
                <div className={'Gallery'}>
                    <Gallery photos={images} renderImage={imageRenderer} />
                </div>
            </div>
        </MainContainer>
    )
}

export default CreateEvent;
