import React from "react";
import ReactDOM from "react-dom";
import "./styles/index.css";
import App from "./components/App";
import * as serviceWorker from "./serviceWorker";


import "bootstrap/dist/css/bootstrap.min.css";


ReactDOM.render(
    <>
        <App />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    </>,
  document.getElementById("root")
);

serviceWorker.unregister();
