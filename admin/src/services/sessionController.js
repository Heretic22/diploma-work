function createSession(token) {
    const session = localStorage.getItem('session');
    if(session){
        throw new Error('session already exist')
    }
    localStorage.setItem('session', token);
}

function deleteSession() {
    const session = localStorage.getItem('session');
    if(!session){
        throw new Error('session already delete')
    }
    localStorage.removeItem('session');
}

function getSession() {
    return localStorage.getItem('session');
}

module.exports = {
    createSession,
    deleteSession,
    getSession
};