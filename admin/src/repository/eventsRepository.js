import {observable} from "mobx";

class EventsRepository {
    @observable events=[];
    @observable selectedEvent={};
}

export default new EventsRepository();
