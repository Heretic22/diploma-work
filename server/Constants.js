const eventStatusTypes = { published: 'PUBLISHED', active: 'ACTIVE', ended: 'ENDED'};

module.exports={
    eventStatusTypes
};