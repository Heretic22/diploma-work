const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
const mongoose = require("mongoose");


const passport = require("./passport_local_strategy");
const auth = require("./routes/auth");
const schools = require("./routes/schools");
const events = require("./routes/events");
// const db = require('./db');

const port = 8443;


const MONGO_URI = 'mongodb+srv://admin:admin12345@cluster0-cgrga.mongodb.net/diploma?retryWrites=true&w=majority';


mongoose
    .connect(MONGO_URI, {useNewUrlParser: true})
    .then(console.log(`MongoDB connected ${MONGO_URI}`))
    .catch(err => console.log(err));


let app = express();
app.use(cors());
app.options('*', cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(
    session({
        secret: "very secret this is",
        resave: false,
        saveUninitialized: false,
        store: new MongoStore({ mongooseConnection: mongoose.connection, touchAfter: 24 * 3600})
    })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Routes
app.use("/api/auth", auth);
app.use("/api/schools", schools);
app.use("/api/events", events);

app.get('/test', (req, res) => res.send(JSON.stringify('Hello World!')));

app.post('/test-post', (req, res) => {
    console.log(req.body);
    // db.showCollectionPrincipals();
    res.send(JSON.stringify('POST'))
});

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))