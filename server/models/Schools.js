const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const SchoolsSchema = new mongoose.Schema(
    {
        principal: { type: Schema.Types.ObjectId, ref: 'Principals' },
        fullName: {
            type: String
        },
        requiredHours: {
            type: Number
        },
    },
    { strict: false }
);

module.exports = Schools = mongoose.model("schools", SchoolsSchema);