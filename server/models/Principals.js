const mongoose = require("mongoose");

const PrincipalsSchema = new mongoose.Schema(
    {
        fullName: {
            type: String
        },
        email: {
            type: String,
            required: true,
            unique: true
        },
        password: {
            type: String
        },
    },
    { strict: false }
);

module.exports = Principals = mongoose.model("principals", PrincipalsSchema);