const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const EventsSchema = new mongoose.Schema(
    {
        school: { type: Schema.Types.ObjectId, ref: 'Schools' },
        name: {
            type: String
        },
        address: {
            type: String
        },
        start:{
            type: Date
        },
        end:{
            type: Date
        },
        category:{
            type: String
        },
        photoUrl:{
            type: String
        },
        description:{
            type: String
        },
        status:{
            type: String
        },
    },
    { strict: false }
);

module.exports = Events = mongoose.model("events", EventsSchema);