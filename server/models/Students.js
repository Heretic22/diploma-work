const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const StudentsSchema = new mongoose.Schema(
    {
        school: { type: Schema.Types.ObjectId, ref: 'Schools' },
        fullName: {
            type: String
        },
        totalHours: {
            type: Number
        },
        email: {
            type: String,
            required: true,
            unique: true
        },
        password: {
            type: String
        },
    },
    { strict: false }
);

module.exports = Students = mongoose.model("students", StudentsSchema);