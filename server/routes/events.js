const middleware = require("./middleware");
const constants = require("../Constants");
const express = require("express");
const Events = require("../models/Events");
const Schools = require("../models/Schools");


const router = express.Router();



router.post("/create",middleware.validateToken, async (req, res, next) => {
    const token = req.headers.authorization;
    try {
        const school = await Schools.findOne({principal:token});
        if (!school) {
            return res.status(400).json({error: "Create school"});
        }
        const newEvent = new Events({
            name:req.body.name,
            address:req.body.address,
            start:req.body.startDate,
            end:req.body.endDate,
            photoUrl:req.body.photoUrl,
            description:req.body.description,
            school:school._id,
            status:constants.eventStatusTypes.published,
        });
        const data = await newEvent.save();
        res.send(JSON.stringify(data))
    } catch (e) {
        return res.status(400).json({ error:e.message});
    }
});

router.get('/school-events',middleware.validateToken, async (req, res, next)=>{
    const token = req.headers.authorization;
    try {
        const school = await Schools.findOne({principal:token});
        if (!school) {
            return res.status(400).json({error: "Create school"});
        }
        Events.find({school:school._id})
            .then(data=>{
                const events = data.map(item=>{
                    return{
                        status:item.status,
                        name:item.name,
                        photoUrl:item.photoUrl,
                        start:item.start,
                        id:item._id
                    }
                });
                console.log(events);
                res.send(JSON.stringify(events))
            });
    } catch (e) {
        return res.status(400).json({ error:e.message});
    }
});

router.get('/get-event-by-id/:id',middleware.validateToken,async(req, res, next)=>{
    console.log("choice id is " +req.params.id);
    const token = req.headers.authorization;
    const eventId = req.params.id;
    try {
        const school = await Schools.findOne({principal: token});
        if (!school) {
            return res.status(400).json({error: "Create school"});
        }
        Events.find({_id:eventId})
            .then(data=>{
                res.send(JSON.stringify(data))
            });
    } catch (e) {
        return res.status(400).json({ error:e.message});
    }
});


module.exports = router;