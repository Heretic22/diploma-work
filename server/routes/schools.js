const middleware = require("./middleware");
const express = require("express");
const Schools = require("../models/Schools");


const router = express.Router();


router.post("/create",middleware.validateToken, async (req, res, next) => {
    try {
        const newSchool = new Schools({fullName:req.body.fullName, requiredHours:req.body.requiredHours, principal:user._id});
        const data = await newSchool.save();
        res.send(JSON.stringify(data))
    } catch (e) {
        return res.status(400).json({ error:e.message});
    }
});


module.exports = router;