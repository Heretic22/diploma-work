const checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
const Principals = require("../models/Principals");



const validateToken = async (req, res, next) =>{
    const token = req.headers.authorization;
    if(!token || !checkForHexRegExp.test(token)){
        return res.status(401).json({ error:"Not authorized"});
    }
    try {
        const user = await Principals.findById(token);
        if (!user) {
            return res.status(401).json({error: "Not authorized"});
        }
        next();
    } catch (e) {
        return res.status(400).json({ error:e.message});
    }
};

module.exports = {
    validateToken
};