const express = require("express");
const router = express.Router();
const passport = require("passport");
const bcrypt = require("bcryptjs");
const Principals = require("../models/Principals");
const Schools = require("../models/Schools");


router.post("/login", (req, res, next) => {
    passport.authenticate("local", function(err, user, info) {
        console.log(err, user, info);
        if (err) {
            return res.status(400).json({ errors: err });
        }
        if (!user) {
            return res.status(400).json({ errors:info?info.message:"No user found"});
        }
        req.logIn(user, async function(err) {
            if (err) {
                return res.status(400).json({ errors: err });
            }
            const school = await Schools.findOne({principal:user.id});
            if(school) return res.status(200).json({ success: user.id, school:true});
            return res.status(200).json({ success: user.id, school:false});
        });
    })(req, res, next);
});

router.post("/register", (req, res, next) => {
    passport.authenticate("local", function(err, user, info) {
        console.log(err, user, info);
        if (err) {
            return res.status(400).json({ errors: err });
        }
        if(user){
            return res.status(400).json({ errors: 'you already register' });
        }
        if (!user) {
            if (!info) {
                console.log(req.body);
                const newUser = new Principals({email:req.body.email, password:req.body.password, fullName:req.body.fullName});
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        if (err) throw err;
                        newUser.password = hash;
                        newUser
                            .save()
                            .then(user => {
                                return res.status(200).json({ success: user.id});
                            })
                            .catch(err => {
                                return res.status(400).json({ errors: err });
                            });
                    });
                });
            } else if(info.message === 'Wrong password'){
                return res.status(400).json({ errors: 'you already register' });
            }
        }

    })(req, res, next);
});

module.exports = router;